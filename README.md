# Options files for filtered MC productions

⛔️⛔️⛔️ There shouldn't be a need to interact with this package directly! ⛔️⛔️⛔️

⛔️⛔️⛔️ See [`LbMCSubmit`](https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit) for details. ⛔️⛔️⛔️

This data package provides options files for submitting filtered MC productions for Run 1 and 2 data.
Files are automatically created by `LbMCSubmit` during the submission process.
